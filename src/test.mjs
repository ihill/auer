import time_in_lux from "./letztime.mjs";

if (process.argv[2]) {
  var hour = parseInt(process.argv[2])
} else {
  var hour = 7;
}

function format(hour, min) {
  var hr = hour.toString().padStart(2, "0");
  var mn = min.toString().padStart(2, "0");
  return hr + ":" + mn
}

for (var minute = 0; minute < 60; minute += 1) {
  var dt = format(hour, minute);
  console.log(dt + " = " + time_in_lux(dt));
}

