var numeral_list = {
    // Not a true list of Luxembourgish numbers, this already
    // takes into account some specifics such as noun gender.
    0: "zwielef",
    1: "eng",
    2: "zwou",
    3: "dräi",
    4: "véier",
    5: "fënnef",
    6: "sechs",
    7: "siwen",
    8: "aacht",
    9: "néng",
    10: "zéng",
    11: "eelef",
    12: "zwielef",
    13: "dräizéng",
    14: "véierzéng",
    15: "Véierel",
    16: "siechzéng",
    17: "siwwenzéng",
    18: "uechtzéng",
    19: "nonzéng",
    20: "zwanzeg",
}

function numeral(number) {
    return numeral_list[number]
}

export default function time_in_lux(time) {
    if (time == "00:00") {
        // Special case: Midnight
        return "Mëtternuecht"
    }

    var preprop = "op"
    var auer = ""
    var minutten = ""
    var [hour_in, min_in] = time.split(":")
    hour_in = parseInt(hour_in)
    min_in = parseInt(min_in)

    if (min_in > 20) {
        // +1 rule
        hour_in += 1
    }

    if (hour_in > 12) {
        // We're not interested in the 24 hour clock
        hour_in -= 12
    }

    if (min_in == 0) {
        auer = "Auer"
        preprop = ""
        min_in = ""
    } else if (min_in <= 20) {
        preprop = "op"
    } else if (min_in > 20 && min_in < 30) {
        preprop = "vir hallwer"
        min_in = 30 - min_in
    } else if (min_in == 30) {
        preprop = "hallwer"
    } else if (min_in > 30 && min_in < 40) {
        preprop = "op hallwer"
        min_in -= 30
    } else if (min_in >= 40) {
        preprop = "vir"
        min_in = 60 - min_in
    }

    if (min_in % 5 != 0) {
        // Plural
        if (preprop.startsWith("vir")) {
            // n-rule
            minutten = "Minutte"
        } else {
            minutten = "Minutten"
        }
    }

    if (min_in === 1) {
        // Singular
        minutten = "Minutt"
    }

    var result = [
        numeral(min_in),
        minutten,
        preprop,
        numeral(hour_in),
        auer,
    ].filter(Boolean).join(" ")

    // n-rule
    result = result.replace("siwen Minut", "siwe Minut")
    return result
}
