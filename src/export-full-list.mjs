import time_in_lux from "./letztime.mjs";

function format(hour, min) {
  var hr = hour.toString().padStart(2, "0");
  var mn = min.toString().padStart(2, "0");
  return hr + ":" + mn
}

for (var hour = 0; hour < 24; hour += 1) {
  for (var minute = 0; minute < 60; minute += 1) {
    var dt = format(hour, minute);
    console.log(dt + " = " + time_in_lux(dt));
  }
}

